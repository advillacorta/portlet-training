package com.ibm.training.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.ibm.training.dtos.FlightPriceTracker;
import com.ibm.training.helpers.TtiDbHelper;

public class FlightPriceTrackerAccessor
{
	// SQL Prepared statements
	private static final String SELECT_ALL_FLIGHTPRICE =
		"SELECT * FROM app.flightPriceTracker ORDER BY originationCity, destinationCity, flightDate";
	private static final String INSERT_FLIGHTPRICE =
		"INSERT INTO app.flightPriceTracker (originationCity, destinationCity, airline, flightNumber, flightDate, cost) values (?, ?, ?, ?, ?, ?)";
	
	// SQL access variables
	private Connection con = null;
	private PreparedStatement ps = null;
	private ResultSet res = null;
	
	public FlightPriceTrackerAccessor()
	{
		
	}
	
	public ArrayList<FlightPriceTracker> getAllFlightPrices()
	{
		ArrayList<FlightPriceTracker> flightPrices =
			new ArrayList<FlightPriceTracker>();
		getConnection();
		
		try
		{
			ps = con.prepareStatement(SELECT_ALL_FLIGHTPRICE);
			res = ps.executeQuery();
			while (res.next())
			{
				load(flightPrices);
			}
			closeDatabase();
		}
		
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		
		return flightPrices;
	}
	
	
	private void load(ArrayList flightPrices)
		throws SQLException
	{
		FlightPriceTracker flightPrice = new FlightPriceTracker();
		flightPrice.setOriginationCity(res.getString("originationCity"));
		flightPrice.setDestinationCity(res.getString("destinationCity"));
		flightPrice.setAirline(res.getString("airline"));
		flightPrice.setFlightNumber(res.getInt("flightNumber"));
		flightPrice.setFlightDate(res.getDate("flightDate"));
		flightPrice.setCost(res.getBigDecimal("cost"));
		flightPrices.add(flightPrice);
	}
	
	private void getConnection()
	{
		try
		{
			con = TtiDbHelper.getDataSource().getConnection();
		}
		catch (SQLException e)
		{

		}
	}

	private void closeDatabase()
	{
		closeResultSet();
		closePreparedStatement();
		closeConnection();
	}

	private void closeConnection()
	{
		try
		{
			if (con != null)
			{
				con.close();
			}
		}
		catch (SQLException e)
		{

		}
	}

	private void closePreparedStatement()
	{
		try
		{
			if (ps != null)
			{
				ps.close();
			}
		}
		catch (SQLException e)
		{

		}
	}

	private void closeResultSet()
	{
		try
		{
			if (res != null)
			{
				res.close();
			}
		}
		catch (SQLException e)
		{

		}
	}
}