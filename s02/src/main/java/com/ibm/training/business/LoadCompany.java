package com.ibm.training.business;

import java.util.ResourceBundle;

public class LoadCompany 
{
	public LoadCompany() 
	{
		// TODO Auto-generated constructor stub
	}
	
	public Company getCompany()
	{
		Company company = new Company();
		company.setCompanyName("Travel Tips International Inc.");
		company.setAddress1("123 North Main");
		company.setAddress2("Suite 600");
		company.setCity("Southfield");
		company.setState("MI");
		company.setZip("48075");
		company.setCountry("United States");
		return company;
	}
	
	public Company getCompany(ResourceBundle portletBundle)
	{
		Company company = new Company();
		company.setCompanyName
		(portletBundle.getString("companyInfo.companyName"));
		company.setAddress1
		(portletBundle.getString("companyInfo.address1"));
		company.setAddress2
		(portletBundle.getString("companyInfo.address2"));
		company.setCity
		(portletBundle.getString("companyInfo.city"));
		company.setState
		(portletBundle.getString("companyInfo.state"));
		company.setZip
		(portletBundle.getString("companyInfo.zip"));
		company.setCountry
		(portletBundle.getString("companyInfo.country"));
		return company;
	}
}