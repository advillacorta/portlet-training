package com.ibm.training.dtos;

import java.math.BigDecimal;
import java.sql.Date;

public class FlightPriceTracker
{	
	private String originationCity;
	private String destinationCity;
	private String airline;
	private int flightNumber;
	private Date flightDate;
	private BigDecimal cost;

	public FlightPriceTracker()
	{
		// TODO Auto-generated constructor stub
	}

	public String getOriginationCity()
	{
		return originationCity;
	}

	public void setOriginationCity(String originationCity)
	{
		this.originationCity = originationCity;
	}

	public String getDestinationCity()
	{
		return destinationCity;
	}

	public void setDestinationCity(String destinationCity)
	{
		this.destinationCity = destinationCity;
	}

	public String getAirline()
	{
		return airline;
	}

	public void setAirline(String airline)
	{
		this.airline = airline;
	}

	public int getFlightNumber()
	{
		return flightNumber;
	}

	public void setFlightNumber(int flightNumber)
	{
		this.flightNumber = flightNumber;
	}

	public Date getFlightDate()
	{
		return flightDate;
	}

	public void setFlightDate(Date flightDate)
	{
		this.flightDate = flightDate;
	}

	public BigDecimal getCost()
	{
		return cost;
	}

	public void setCost(BigDecimal cost)
	{
		this.cost = cost;
	}
}