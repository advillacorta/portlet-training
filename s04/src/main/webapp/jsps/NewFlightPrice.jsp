<%@taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c_rt"%>
<%@taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt_rt"%>

<portlet:defineObjects />

<fmt_rt:setBundle
	basename="com.ibm.training.destinationinfoportlets.nl.FlightPriceTrackerPortletResource"/>

<form method="post" action="<portlet:actionURL />">
	<table border="0" cellpadding="3" cellspacing="0">
		<tbody>
			<tr>
				<td><fmt_rt:message key="flightPriceTracker.originationCity" /></td>
				<td><input type="text" name="originationCity" size="3" maxlength="3"></td>
			</tr>
			<tr>
				<td><fmt_rt:message key="flightPriceTracker.destinationCity" /></td>
				<td><input type="text" name="destinationCity" size="3" maxlength="3"></td>
			</tr>
			<tr>
				<td><fmt_rt:message key="flightPriceTracker.airline" /></td>
				<td><input type="text" name="airline" size="30" maxlength="50"></td>
			</tr>
			<tr>
				<td><fmt_rt:message key="flightPriceTracker.flightNumber" /></td>
				<td><input type="text" name="flightNumber" size="3" maxlength="4"></td>
			</tr>
			<tr>
				<td><fmt_rt:message key="flightPriceTracker.flightDate" /></td>
				<td><input type="text" name="flightDate" size="10" maxlength="10"></td>
			</tr>
			<tr>
				<td><fmt_rt:message key="flightPriceTracker.cost" /></td>
				<td><input type="text" name="cost" size="8" maxlength="8"></td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<input type="submit" name="Submit" class="wpsButtonText"
						value="<fmt_rt:message key="flightPriceTracker.addButton" />" />
				</td>
			</tr>
		</tbody>
	</table>
</form>