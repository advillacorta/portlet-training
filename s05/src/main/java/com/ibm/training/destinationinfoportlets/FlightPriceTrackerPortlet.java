package com.ibm.training.destinationinfoportlets;

import java.io.IOException;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.GenericPortlet;
import javax.portlet.PortletException;
import javax.portlet.PortletMode;
import javax.portlet.PortletModeException;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.PortletSession;
import javax.portlet.ReadOnlyException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.portlet.ValidatorException;

import com.ibm.training.daos.FlightPriceTrackerAccessor;
import com.ibm.training.dtos.FlightPriceTracker;

public class FlightPriceTrackerPortlet extends GenericPortlet 
{
	@Override
	public void init() throws PortletException 
	{
		// TODO Auto-generated method stub
		super.init();
	}
	
	/**
	 * Serve up the <code>view</code> mode.
	 * 
	 * @see javax.portlet.GenericPortlet#doView(javax.portlet.RenderRequest, javax.portlet.RenderResponse)
	 */
	protected void doView(RenderRequest request, RenderResponse response) throws PortletException, IOException 
	{
		response.setContentType(request.getResponseContentType());
		
		String targetJsp = chooseViewTask(request);
		PortletRequestDispatcher rd = getPortletContext().getRequestDispatcher(targetJsp);
		rd.include(request, response);
	}
	
	private String chooseViewTask(RenderRequest request)
	{
		String nextTask = request.getParameter("nextTask");
		
		if (nextTask == null)
		{
			FlightPriceTrackerAccessor flightPriceAccessor = new FlightPriceTrackerAccessor();
			
			PortletPreferences prefs = request.getPreferences();
			String preferredDepartureCity = prefs.getValue("preferredDepartureCity", "");
			Collection<FlightPriceTracker> flightPrices = null;
			if (preferredDepartureCity.equals(""))
			{
				flightPrices = flightPriceAccessor.getAllFlightPrices();
			}
			else
			{
				flightPrices = flightPriceAccessor.
						getFlightPricesByCity(preferredDepartureCity);
			}
			
			request.setAttribute("flightPrices", flightPrices);
			
			String targetJsp = "/jsps/FlightPriceList.jsp";
			PortletSession session = request.getPortletSession();
			session.setAttribute("targetJsp", targetJsp);
			return targetJsp;
		}
		
		if (nextTask.equals("DisplayNewFlightPriceForm"))
		{
			String targetJsp = "/jsps/NewFlightPrice.jsp";
			PortletSession session = request.getPortletSession();
			session.setAttribute("targetJsp", targetJsp);
			return targetJsp;
		}
		
		String targetJsp = "/jsps/FlightPriceList.jsp";
		PortletSession session = request.getPortletSession();
		session.setAttribute("targetJsp", targetJsp);
		return targetJsp;
	}
	
	/**
	 * Serve up the <code>edit</code> mode.
	 * 
	 * @see javax.portlet.GenericPortlet#doEdit(javax.portlet.RenderRequest, javax.portlet.RenderResponse)
	 */
	protected void doEdit(RenderRequest request, RenderResponse response) throws PortletException, IOException 
	{
		PortletSession session = request.getPortletSession();
		String targetJsp = "/jsps/FlightPriceListPreferences.jsp";
		session.setAttribute("targetJsp", targetJsp);
		
		PortletPreferences prefs = request.getPreferences();
		String preferredDepartureCity =prefs.getValue("preferredDepartureCity", "");
		request.setAttribute("preferredDepartureCity",preferredDepartureCity);
		
		PortletRequestDispatcher rd = getPortletContext().getRequestDispatcher(targetJsp);
		rd.include(request, response);
	}
	
	/**
	 * Serve up the <code>help</code> mode.
	 * 
	 * @see javax.portlet.GenericPortlet#doHelp(javax.portlet.RenderRequest, javax.portlet.RenderResponse)
	 */
	protected void doHelp(RenderRequest request, RenderResponse response) throws PortletException, IOException 
	{

	}
	
	/**
	 * Process an action request.
	 * 
	 * @see javax.portlet.Portlet#processAction(javax.portlet.ActionRequest, javax.portlet.ActionResponse)
	 */
	public void processAction(ActionRequest request, ActionResponse response) throws PortletException, IOException 
	{
		PortletSession session = request.getPortletSession();
		String currentJsp = (String)session.getAttribute("targetJsp");
		
		if (currentJsp.equals("/jsps/NewFlightPrice.jsp"))
		{
			processNewFlightPrice(request);
			return;
		}
		if (currentJsp.equals("/jsps/FlightPriceListPreferences.jsp"))
		{
			processFlightPriceListPreferences(request, response);
			return;
		}
	}

	private void processNewFlightPrice(ActionRequest request) 
	{
		Map<String, String[]> parameterList = request.getParameterMap();
		FlightPriceTrackerAccessor flightPriceAccessor = new FlightPriceTrackerAccessor();
		Map errorMessages = flightPriceAccessor.insert(parameterList);
	}
	
	private void processFlightPriceListPreferences(ActionRequest request, ActionResponse response)
	throws IOException, ReadOnlyException, PortletModeException 
	{
		PortletPreferences prefs = request.getPreferences();
		Enumeration<String> failedKeys = null;
		prefs.setValue("preferredDepartureCity",
		request.getParameter("preferredDepartureCity"));
		try
		{
			prefs.store();
		}
		catch (ValidatorException e)
		{
			failedKeys = e.getFailedKeys();
		}
		if (failedKeys == null)
		{
			response.setPortletMode(PortletMode.VIEW);
			return;
		}
	}
	
	/**
	 * Process a serve Resource request.
	 * 
	 * @see javax.portlet.Portlet#serveResource(javax.portlet.ResourceRequest, javax.portlet.ResourceResponse)
	 */
	public void serveResource(ResourceRequest request, ResourceResponse response) throws PortletException, java.io.IOException 
	{
		String resourceID = request.getResourceID();
		if (resourceID.equals("resourceID")) 
		{
			// Insert code for serving the resource 
		}
	}
}
