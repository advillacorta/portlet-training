package com.ibm.training.destinationinfoportlets;

import java.util.HashSet;
import java.util.Set;

import javax.portlet.PortletPreferences;
import javax.portlet.PreferencesValidator;
import javax.portlet.ValidatorException;

public class FlightPricePreferencesValidator implements PreferencesValidator 
{
	private static final String airportCodes = "ATL DTW DFW IAH LAX HNL LGA";

	@Override
	public void validate(PortletPreferences prefs) throws ValidatorException 
	{
		Set<String> failedKeys = new HashSet<String>();
		if (!airportCodes.contains(prefs.getValue("preferredDepartureCity", "")))
		{
		failedKeys.add("preferredDepartureCity");
		}
		if (failedKeys.isEmpty())
		{
		return;
		}
		throw new ValidatorException("Error Validating Preferences",failedKeys);
	}
}