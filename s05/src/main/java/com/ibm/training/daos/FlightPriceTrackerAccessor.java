package com.ibm.training.daos;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.ibm.training.dtos.FlightPriceTracker;
import com.ibm.training.helpers.TtiDbHelper;

public class FlightPriceTrackerAccessor
{
	// SQL Prepared statements
	private static final String SELECT_FLIGHTPRICES_BY_CITY =
			"SELECT * FROM app.flightPriceTracker WHERE originationCity=?ORDER BY destinationCity, flightDate";
	private static final String SELECT_ALL_FLIGHTPRICE =
		"SELECT * FROM app.flightPriceTracker ORDER BY originationCity, destinationCity, flightDate";
	private static final String INSERT_FLIGHTPRICE =
		"INSERT INTO app.flightPriceTracker (originationCity, destinationCity, airline, flightNumber, flightDate, cost) values (?, ?, ?, ?, ?, ?)";
	
	// SQL access variables
	private Connection con = null;
	private PreparedStatement ps = null;
	private ResultSet res = null;
	
	public FlightPriceTrackerAccessor()
	{
		
	}
	
	public ArrayList<FlightPriceTracker> getAllFlightPrices()
	{
		ArrayList<FlightPriceTracker> flightPrices =
			new ArrayList<FlightPriceTracker>();
		getConnection();
		
		try
		{
			ps = con.prepareStatement(SELECT_ALL_FLIGHTPRICE);
			res = ps.executeQuery();
			while (res.next())
			{
				load(flightPrices);
			}
			closeDatabase();
		}
		
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		
		return flightPrices;
	}
	
	
	private void load(ArrayList flightPrices)
		throws SQLException
	{
		FlightPriceTracker flightPrice = new FlightPriceTracker();
		flightPrice.setOriginationCity(res.getString("originationCity"));
		flightPrice.setDestinationCity(res.getString("destinationCity"));
		flightPrice.setAirline(res.getString("airline"));
		flightPrice.setFlightNumber(res.getInt("flightNumber"));
		flightPrice.setFlightDate(res.getDate("flightDate"));
		flightPrice.setCost(res.getBigDecimal("cost"));
		flightPrices.add(flightPrice);
	}
	
	public Map<String, String> insert(Map<String,String[]> parameterList)
	{
		HashMap<String, String> errorMessages = new HashMap<String,String>();
		getConnection();
		try
		{
			String [] parameterValue = null;
			ps = con.prepareStatement(INSERT_FLIGHTPRICE);

			parameterValue = (String [])(parameterList.get("originationCity"));
			ps.setString(1, parameterValue[0]);

			parameterValue = (String [])(parameterList.get("destinationCity"));
			ps.setString(2, parameterValue[0]);

			parameterValue = (String [])(parameterList.get("airline"));
			ps.setString(3, parameterValue[0]);

			parameterValue = (String [])(parameterList.get("flightNumber"));
			int flightNumber =
				Integer.parseInt(parameterValue[0]);
			ps.setInt(4, flightNumber);

			parameterValue = (String [])(parameterList.get("flightDate"));
			SimpleDateFormat sdf = new SimpleDateFormat("M/d/yy");
			java.util.Date flightDate = null;
			try
			{
				flightDate = sdf.parse(parameterValue[0]);
			}

			catch (ParseException e)
			{
				errorMessages.put("flightDate", "Invalid Date Format");
			}
			
			ps.setDate(5, new Date(flightDate.getTime()));

			parameterValue = (String [])(parameterList.get("cost"));
			BigDecimal cost = new BigDecimal(parameterValue[0]);
			ps.setBigDecimal(6, cost);
			
			ps.execute(); 
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		closeDatabase();
		return errorMessages;
	}
	
	public ArrayList<FlightPriceTracker> getFlightPricesByCity(String departureCity)
	{
		ArrayList<FlightPriceTracker> flightPrices =
		new ArrayList<FlightPriceTracker>();
		getConnection();
		try
		{
			ps = con.prepareStatement(SELECT_FLIGHTPRICES_BY_CITY);
			ps.setString(1, departureCity);
			res = ps.executeQuery();
		while (res.next())
		{
			load(flightPrices);
		}
		closeDatabase();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return flightPrices;
	}
	
	private void getConnection()
	{
		try
		{
			con = TtiDbHelper.getDataSource().getConnection();
		}
		catch (SQLException e)
		{

		}
	}

	private void closeDatabase()
	{
		closeResultSet();
		closePreparedStatement();
		closeConnection();
	}

	private void closeConnection()
	{
		try
		{
			if (con != null)
			{
				con.close();
			}
		}
		catch (SQLException e)
		{

		}
	}

	private void closePreparedStatement()
	{
		try
		{
			if (ps != null)
			{
				ps.close();
			}
		}
		catch (SQLException e)
		{

		}
	}

	private void closeResultSet()
	{
		try
		{
			if (res != null)
			{
				res.close();
			}
		}
		catch (SQLException e)
		{

		}
	}
}