package com.ibm.training.helpers;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class TtiDbHelper
{
	private static DataSource ds = null;
	
	private static final String DATASOURCE_JNDI_NAME = "jdbc/ttidb";

	// Private constructor prevents instantiation
	private TtiDbHelper()
	{
		
	}

	/*
	 *  
	 */
	public static synchronized DataSource getDataSource()
	{
		// Return DataSource if already available
		if (ds != null)
		{
			return ds;
		}
		
		try
		{
			// DataSource not available: look it up
			InitialContext ic = new InitialContext();
			ds = (DataSource)ic.lookup(DATASOURCE_JNDI_NAME);
		}
		catch (NamingException e)
		{
			// DataSource not available
			e.printStackTrace();
		}
		
		return ds;
	}
}