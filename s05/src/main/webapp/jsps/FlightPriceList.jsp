<%@page language="java" 
contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" session="true"%>
<%@taglib prefix="c_rt" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib prefix="fmt_rt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@taglib uri="http://java.sun.com/portlet" prefix="portlet"%>

<portlet:defineObjects/>

<fmt_rt:setBundle basename="com.ibm.training.destinationinfoportlets.nl.FlightPriceTrackerPortletResource"/>

<table border="1" cellpadding="3">
 <thead>
  <tr>
   <th>
    <fmt_rt:message key="flightPriceTracker.originationCity"/>
   </th>
   <th>
    <fmt_rt:message key="flightPriceTracker.destinationCity"/>
   </th>
   <th>
    <fmt_rt:message key="flightPriceTracker.airline"/>
   </th>
   <th>
    <fmt_rt:message key="flightPriceTracker.flightNumber"/>
   </th>
   <th>
    <fmt_rt:message key="flightPriceTracker.flightDate"/>
   </th>
   <th>
    <fmt_rt:message key="flightPriceTracker.cost"/>
   </th>
  </tr>
 </thead>
 <tbody>
  <c_rt:forEach items="${requestScope.flightPrices}" var="flightPrice">
   <tr>
    <td>${flightPrice.originationCity}</td>
    <td>${flightPrice.destinationCity}</td>
    <td>${flightPrice.airline}</td>
    <td>${flightPrice.flightNumber}</td>
    <td><fmt_rt:formatDate value="${flightPrice.flightDate}"/></td>
    <td align="right"><fmt_rt:formatNumber type="currency" value="${flightPrice.cost}"/></td>
   </tr>
  </c_rt:forEach>
  <tr>
   <td colspan="6" align="center">
    <a href="<portlet:renderURL><portlet:param name="nextTask" value="DisplayNewFlightPriceForm"/></portlet:renderURL>">
     <fmt_rt:message key="flightPriceTracker.addLink"/>
    </a>
   </td>
  </tr>
 </tbody>
</table>