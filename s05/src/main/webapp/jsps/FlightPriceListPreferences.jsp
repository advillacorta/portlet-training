
<%-- FlightPriceListPreferences.jsp --%>
<%-- (c) Copyright IBM Corp. 2012, 2014 --%>
<%-- US Government Users Restricted Rights - Use duplication --%>
<%-- or disclosure restricted by GSA ADP Schedule Contract with IBM Corp --%>



<%@taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@taglib uri="http://www.ibm.com/xmlns/prod/websphere/portal/v6.1/portlet-client-model" prefix="portlet-client-model"%>
<%@taglib uri="http://java.sun.com/portlet" prefix="portletx"%>
<%@taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt_rt"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" session="false"%>

<portlet-client-model:init>
	<portlet-client-model:require module="ibm.portal.xml.*" />
	<portlet-client-model:require module="ibm.portal.portlet.*" />
</portlet-client-model:init>
<portlet:defineObjects />

<fmt_rt:setBundle basename="com.ibm.training.destinationinfoportlets.nl.FlightPriceTrackerPortletResource"/>

<form method="post" action="<portlet:actionURL/>">
	<table border="0" cellpadding="3">
		<tbody>
			<tr>
				<td>
					<fmt_rt:message key="flightPriceTracker.preferredDepartureCity"/>
				</td>
				<td>
					<input type="text" name="preferredDepartureCity" size="3" maxlength="3"
						value="${requestScope.preferredDepartureCity}" }/>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="submit" name="submit" class="wpsButtonText" 
						value="<fmt_rt:message key="flightPriceTracker.save" />"/>
				</td>
			</tr>
		</tbody>
	</table>
</form>	