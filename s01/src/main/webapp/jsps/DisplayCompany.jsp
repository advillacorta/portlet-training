<%-- DisplayCompany.jsp --%>
<%-- (c) Copyright IBM Corp. 2012, 2014 --%>
<%-- US Government Users Restricted Rights-Use duplication --%>
<%-- or disclosure restricted by GSA ADP Schedule Contract with IBM Corp --%>




<%@taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@taglib uri="http://www.ibm.com/xmlns/prod/websphere/portal/v6.1/portlet-client-model" prefix="portlet-client-model"%>
<%@taglib uri="http://java.sun.com/portlet" prefix="portletx"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c_rt"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" session="false"%>
<portlet-client-model:init>
	<portlet-client-model:require module="ibm.portal.xml.*" />
	<portlet-client-model:require module="ibm.portal.portlet.*" />
</portlet-client-model:init>
<portlet:defineObjects />
<c_rt:set var="company" value="${requestScope.company}" />

<table border="0" cellpadding="3">
	<tbody>
		<tr>
			<td><b>Company Name: </b></td>
			<td>${company.companyName}</td>
		</tr>
		<tr>
			<td><b>Address:</b></td>
			<td>${company.address1}</td>
		</tr>
		<tr>
			<td></td>
			<td>${company.address2}</td>
		</tr>
		<tr>
			<td><b>City, State, Zip</b></td>
			<td>${company.city}, ${company.state} ${company.zip}</td>
		</tr>
		<tr>
			<td><b>Country:</b></td>
			<td>${company.country}</td>
		</tr>
	</tbody>
</table>