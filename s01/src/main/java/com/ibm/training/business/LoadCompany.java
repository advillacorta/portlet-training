package com.ibm.training.business;

public class LoadCompany 
{
	public LoadCompany() 
	{
		// TODO Auto-generated constructor stub
	}
	
	public Company getCompany()
	{
		Company company = new Company();
		company.setCompanyName("Travel Tips International Inc.");
		company.setAddress1("123 North Main");
		company.setAddress2("Suite 600");
		company.setCity("Southfield");
		company.setState("MI");
		company.setZip("48075");
		company.setCountry("United States");
		return company;
	}
}
